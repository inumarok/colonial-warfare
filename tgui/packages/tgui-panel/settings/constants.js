/**
 * @file
 * @copyright 2020 Aleksej Komarov
 * @license MIT
 */

export const SETTINGS_TABS = [
  {
    id: 'general',
    name: 'General',
  },
  {
    id: 'chatPage',
    name: 'Chat Tabs',
  },
];

export const FONTS = [
  'Verdana, Geneva, sans-serif',
  'Arial, Helvetica, sans-serif',
  'Arial Black, Gadget, sans-serif',
  'Comic Sans MS, cursive, sans-serif',
  'Impact, Charcoal, sans-serif',
  'Lucida Sans Unicode, Lucida Grande, sans-serif',
  'Tahoma, Geneva, sans-serif',
  'Trebuchet MS, Helvetica, sans-serif',
  'Courier New, Courier, monospace',
  'Lucida Console, Monaco, monospace',
  'Papyrus, cursive, sans-serif',
];
